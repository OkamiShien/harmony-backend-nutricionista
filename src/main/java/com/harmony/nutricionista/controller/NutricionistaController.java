package com.harmony.nutricionista.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nutricionista")
public class NutricionistaController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(NutricionistaController.class);
	
	@RequestMapping(value="/nuevareceta", method = RequestMethod.POST)
	public ResponseEntity<byte[]> nuevaReceta(@RequestBody String name){
		return new ResponseEntity<byte[]>(HttpStatus.CREATED);
	}

}
