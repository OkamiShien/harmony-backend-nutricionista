package com.harmony.nutricionista;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	protected Application() {
		//do Nothing
	}
	
	public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
